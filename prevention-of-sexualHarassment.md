
# Prevention of Sexual Harassment

## Behaviour that causes Sexual Harassment


Any unwelcome verbal,visual, or physical conduct of a sexual nature that is severe or pervasive and affects working conditions or creates a hostile work environment is said to be Sexual Harassment

### Three forms of Sexual Harassment :

 1. Verbal Harassment
 2. Visual Harassment
 3. Physical Harassment

####  Verbal Harassment :

* Comments about clothing, a person's body, sexual or gender based jokes or remarks.
* Requesting sexual favours or repeatedly asking a person out.
* It also includes sexual innuendos,threats,spreading rumors about a person's personal or sexual life.


#### Visual Harassment :
 * It includes obscene poster,drawing/pictures,screensavers,cartoons,emails or text of sexual nature.

#### Physical Harassment :
 * It includes sexual assault, Impeding or blocking movement, Inappropriate touching such as kissing,hugging,patting,stroking or rubbing,sexual gesturing,leering or staring.


## Two categories of Sexual Harassment
 1. Quid Pro Quo
 2. Hostile Work Environment


### Quid Pro Quo :

This happens when an emplyer or supervisor uses job rewards or punishments to coerce an employee into a sexual relationship or sexual act. This includes offering the employee raises or promotion or threatening demotions firing or other penalties for not complying with the request a hostile work environment occurs when employee behaviour interfaces with the work performance of another or creates an intimidating or offensive work place.

### Hostile Work Environment :

Hostile work environment sexual harassment is any conduct directed at an employee because of that employee’s sex that unreasonably interferes with the employee’s work performance or creates an intimidating, hostile or offensive working environment. This usually happens when someone makes repeated sexual comments and makes another employee feel so uncomfortable that their work performance suffers.


## In case I face or witness any incident or repeated incidents of Sexual Harassment behaviour

#### Case:- I face such behaviour

If I am subjected to sexual harassment, I will observe the behavior to determine whether the harasser is doing so knowingly or unknowingly; if they are doing so unknowingly, I will warn them. If they repeat the same behavior, I will inform the higher authorities about their behavior. If they are doing it knowingly, I will immediately inform higher authorities about their behavior. If they do not change their behavior regardless of any warnings from me and higher authorities, then I will file a case against them under the Sexual Harassment Act.


#### Case:- I withness any sexual harassment behaviour

If I witness sexual harassment, I will immediately support the victim and warn the harasser. If the harasser is my coworker, I will warn the harasser to change their behavior. If they do the same, I will inform the higher authorities. If the harasser is my superior, I will notify the appropriate authorities about his or her behavior. Regardless of any warnings, if they continue the same behavior, I will file a case against them under the Sexual Harassment Act.








