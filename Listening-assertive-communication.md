# Listening and Active Communication

## 1. Active Listening

#### Question 1

### What are the steps/strategies to do Active Listening? 

* Active listening is the act of fully hearing and comprehending the meaning of what someone else is saying.
* Avoid getting distracted by our thoughts.
* Focus on the speaker and topic.
* Try not to interrupt the other person let them finish and then respond.
* Show that we are interested to show with body language if another person is talking.
* Take notes during important conversations.

## 2. Reflective Listening

#### Question 2

### According to Fisher's model, what are the key points of Reflective Listening?

* Hearing and understanding what the other person is communicating through words and body language to the best of our ability.
* Responding to the other person by reflecting on the thoughts and feelings we heard in his or
her words, tone of voice, body posture, and gestures.
* It helps the listener understand the message.
* It helps the speaker deliver a clear message.
* It reinforces positive relationships.
* It helps the speaker check the accuracy of their own words and ideas.
* It reduces the odds of two people mistakenly believing they have an understanding.


## Reflection
#### Question 3

### What are the obstacles in your listening process?

* Noise - Any outside noise, such as the sound of machinery operating, phones ringing, or other people conversing, can act as a barrier.
* Visual distractions can be as basic as the activity just outside a window or the events occurring within a neighboring office's glass walls.
* Physical setting: Discomforts such as a troubling temperature, inadequate or nonexistent seats, offensive scents, and a separation between the listener and speaker can all be problematic.
* Stress, mental indifference, boredom, cognitive dissonance, and impatience.

####  Question 4
### What can you do to improve your listening?

* Avoid getting distracted by my thoughts.
* Try not to interrupt the other person.
* I consider eye contact. I turn my face to the conversation member out of politeness.
* I will be alert throughout the conversation.
* I pay attention to nonverbal signs such as body language and tone.
* I allow myself to create a mental image of the information that I am hearing.
* Keep my workspace neat and turn it off.

## 4. Types of Communication

#### Question 5
### When do you switch to Passive communication style in your day to day life?

* I will switch to passive communication When I am in a meeting for the first time.
* I keep quiet when some older individuals advise.

#### Question 6
### When do you switch into Aggressive communication styles in your day to day life?

* When someone speaks in a loud and overbearing voice.
* Criticize me or some persons around me.
* When someone attempts to dominate.
* When someone disrespects my work.
Saying the same statements repeatedly.

#### Question 7
### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

* When I procrastinate on work.
* When people ignore me in public.
* Spreading false gossip.

#### Question 8
### How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life?

* I stand up for myself.
* I say no without any guilt.
* Maintain self-control.
* I rehearse what I want to say.
* I Pay attention to the speaker.
* Genuinely listening from the speaker's point of view.
* Avoid interrupting the speaker.







