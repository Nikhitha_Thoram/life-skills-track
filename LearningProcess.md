# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1

#### What is the Feynman Technique? Explain in 1 line.


The method of learning, reviewing, and explaining the concepts in simple words, so that one can spot their faint areas and effectuate learning is termed as Feynman Technique. 

## Learning How to Learn TED talk by Barbara Oakley

### Question 2

#### In this video, what was the most interesting story or idea for you?

* The most interesting  idea for me is
 how to focus, learn, and remember things well.

There are two methods for dealing with procrastination:

* The first is to push through the pain until it passes.

* The second method is to divert your focus to feel better. However, doing this might have serious negative effects, thus the Pomodoro technique was developed to help.

The Pomodoro Technique: Set a timer for 25 minutes without any interruptions and work for that entire time with our complete attention. After finishing, have some leisurely enjoyment for a short while. The learning process requires this relaxing to stay focused.

It's a common misconception among students that some of their strongest qualities are also their worst ones. Memory-impaired individuals frequently exhibit greater creativity. Other thoughts frequently slip in because they can't keep these concepts in their heads for very long.

So, there is something called "illusions of competence in learning". Exercise within a matter of a few days can increase our ability to both learn and remember. When we do a homework problem, never just work at once and put it away. Test ourselves, and work on that homework problem several times over several days until the solution flows like a song from our mind.

Highlighting important points in pages doesn't go straight to our memory. The most effective technique is simply to look at a page, look away, and see what we can recall. Understanding combined with practice and repetition can truly gain us mastery over what we're learning.


`Don't just follow your passions; broaden your passions, and your life will be enriched beyond measure.`

### Question 3

#### What are active and diffused modes of thinking?

* Active modes
* diffused modes

### Active mode:

Consistent attention on something is called Active mode.
We think deeply about something when we focus on learning.
If we already know about a topic or are not perfect at it, then we can learn efficiently by focusing on it within a short time.
Active mode helps to learn things faster and in an efficient way without any distractions.

### Diffuse mode :

Thinking about a topic in a relaxed state is called Diffuse mode. So, the diffuse mode is considered as a resting state. Diffuse mode helps to understand the topic slowly when we are stuck at some points.

### Pinball Machine Analogy :

If we are focused on something, trying to learn a new concept or solve a problem and we get stuck, we want to turn our attention away from that problem and allow the diffuse modes. These resting states do their work in the background.
A different way of thinking is required if we get stuck or know something about the topic.


## Learn Anything in 20 hours

### Question 4

#### According to the video, what are the steps to take when approaching a new topic? Only mention the points.

* Setting a goal: Setting a goal and knowing its result is the most crucial step in learning anything.

* Break the learning into parts: One cannot learn a huge concept at a time. Learning the concept by breaking it into sub-concepts will help to remember and learn things properly.

* Focus: Constant attention to learn something.
* Teach: Teaching whatever we learn helps to understand the topic in a much better way.
* Test: Test whatever we learn by doing exercises.
* Remove barriers: By removing barriers like procrastination we can learn the concepts easily.
* Practising 20 hours: Consistency effort of practising

### Question 5
#### What are some of the actions you can take going forward to improve your learning process?

* Create a suitable environment to study without any distractions.
* Stay in diffuse mode and once ideas hit move to focussed mode.
* Avoid procrastination.
* Follow the Pomodoro technique to study and learn effectively and create a proper schedule.
* Exercise things until they get familiar.
* Self-examine Myself.
* Understanding things combined with practice and repetition makes us master.















