# Tiny Habits

### 1. Tiny Habits - BJ Fogg
#### Question 1
#### In this video, what was the most interesting story or idea for you?

* Start with a small habit rather than a major shift.
* After establishing a small habit, celebrate.
* Have a reason why we want to form the habit.
* By making small changes to our everyday routine, we can effect significant change.

### 2. Tiny Habits by BJ Fogg - Core Message
#### Question 2
#### How can you use B = MAP to make making new habits easier? What are M, A and P.

B=MAP
* Motivation
* Ability
* prompt
* If task is hard then i need high motivation to achieve the task. So i need to start with easy task.
* Possess habits-triggering behaviours.

#### Question 3
#### Why it is important to "Shine" or Celebrate after each successful completion of habit?

* Celebrating success reinforces the behavior we want to continue. When we associate completing a habit with a positive experience, such as celebration or acknowledgment, we're more likely to repeat that behavior in the future.
* Celebrating achievements can boost motivation and morale. It provides a sense of accomplishment and satisfaction, which can energize us to continue working towards our goals.
*  Celebrating success fosters a positive mindset by focusing on what we've accomplished rather than what we haven't. It helps shift our perspective towards recognizing and appreciating progress, no matter how small.

### 3. 1% Better Every Day Video
#### Question 4
#### In this video, what was the most interesting story or idea for you?
There are four stages of Habit Formation.
* noticing
* wanting
* doing
* liking
* give our goals a time and place to live in the world 
* a quick exercise to notice what is holding us back
* optimize for the starting line. Not the finish line 
* The goal is not to read a book,the goal is to become a reader.

### 4. Book Summary of Atomic Habits
#### Question 5
#### What is the book's perspective about Identity?

* Identity change is the north star of habit change
* The identity which is related to our beliefs.
* Most of us work from outcome to identity rather than identity to outcome.
* The ultimate form of intrinsic motivation "is When a habit becomes part of our identity.
* when we solve problems in terms of outcomes and results, we only solve them temporarily.
But to solve problems in longer term at the system level we need to change our identity.

#### Question 6
#### Write about the book's perspective on how to make a habit easier to do?

A good habit can be cultivated in four stages:
* Cue
* Craving
* Response
* Reward


#### Question 7
#### Write about the book's perspective on how to make a habit harder to do?
* One approach to making a habit harder to do is to increase the friction associated with it. This involves adding obstacles or steps that make it more difficult to engage in the habit.
* Constraints can be powerful tools for making habits harder to do. By imposing limits or restrictions on certain behaviors, we create barriers that discourage their execution. 

### 5. Reflection:
#### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

* I will practice problems on platforms like leetcode and Hackerrank.
* I will try to build simple websites using HTML,CSS,Javascript

#### Question 9:
#### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

spending time on social media
* I will keep the time limit. so it can alert me that I am overusing that app.
* I will try to uninstall the apps that wasting my time.







