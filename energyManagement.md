# Energy Management

### 1. Manage Energy not Time

#### Question 1:
### What are the activities you do that make you relax - Calm quadrant?

* listening Music
* cooking
* movies
* Drawing
* reading books
* spending time with friends

#### Question 2:
### When do you find getting into the Stress quadrant?

* When I need to complete multiple tasks at the same time.
* When I feel guilty of wasting time.
* When I learn some new concepts and if it doesn't sit better.

#### Question 3:
### How do you understand if you are in the Excitement quadrant?

* When I forget all the worries, doesn't think about the next days works and feel less stressed and relaxed.
* When I acheive anything.

### 4. Sleep is your superpower
#### Question 4
### Paraphrase the Sleep is your Superpower video in your own words in brief. 

* Lack of sleep(less than 7 or 8 hours) directly affect the male and female reproductive systems.
* Lack of sleep affects the brain and functions of learning amd memory.
* We need enough sleep before and after study for better memeory.
* Without sleep there is 40% deficit in the function of brain when compared to people with 8 hours sleep.
* It is because of the memory storing site in brain called hippocampus, which got affected in sleep deprived person.
* During the very deepest stages of sleep big powerful brainwaves which are discovered in the study. That shifts memories from short-term storage to permanent long-term storage site within the brain.
* Lack of sleep causes aging, Alzheimer's disease, increase the risk of heart attacks, immune system, cancer, gene activity, cardiovascular disease.
* Avoid naps during day. Regularity is the key point in good and healthy sleep.
* Sleep is a nonnegotiable biological necessity and it is the life-support system.

#### Question 5
### What are some ideas that you can implement to sleep better?

* avoid nap during day time.
* avoid using mobile before sleep.
* Follow regular schedule to sleep.

### 5. Brain Changing Benefits of Exercise
#### Question 6
#### Paraphrase the video - Brain Changing Benefits of Exercise.

* exercise produce brand new cells in the hippocampus,that actually increase it's volume as well as improves long-term memory.
* Boosts our mood and memory.
* Excercise decreases feelings of anxiety.
* It protects our brain from aging and neurodegenerative diseases.

#### Question 7
### What are some steps you can take to exercise more?

* I wake up early and I will do the workout.
* I walk daily.