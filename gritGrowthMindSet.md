# Grit and Growth Mindset

### 1. Grit
#### Question 1
#### Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.


* Grit is passion and preservation for very long-term goals.
* Grit is having stamina
* Grit is sticking with our future.
* Not just for months, but for years, and working hard to make that future a reality.
* It is found in research that grit is the driving force for any individual toward learning.
* To learn anything we need the courage to be on that path which is not easy to pursue, and for that courage, we need to be dedicated to a growth mindset.
* Irrespective of individual skill set, IQ, and other factors, if anyone wants to learn then they just need to be keen to learn and should have strong willpower.


### 2. Introduction to Growth Mindset
#### Question 2
#### Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

* People have two types of mindsets
* These mindsets are really important
when it comes to learning.
* Some people have a fixed mindset and some others have Growth Mindset.
* People with fixed minds believe that skills and intelligence are set.
* People with a fixed mindset believe that you can't or don't have to learn and grow.
* They believe that they are not in control of their abilities.
* People with Growth Mindset believe that skills and intelligence are grown and developed.
* So people who are good at something are good because they built that ability,and people who aren't are not good because they haven't done the work.
* They believe that they are in control
of their abilities.
* people with a growth mindset believe that skills are built.
* and people with a growth mindset do believe in their capacity to learn and grow.

### 3. Understanding Internal Locus of Control
#### Question 3
#### What is the Internal Locus of Control? What is the key point in the video?


* Internal locus of control: They believed they had entire control over the variables determining their achievements. They concluded that their focus and extra effort were whatever caused them to execute well on the tasks. We need to think that we are in control of our lives and are once again accountable for the things that happen to us if we want to feel motivated all the time.
highly motivated to be able to cope with the many setbacks they face every day.
* This webinar addressed the need to maintain a good outlook, be driven, and look for justifications rather than justifications when carrying out our activities.
* The message this film imparted to us was to be doers rather than just explainers and to understand that our largest emotional obstacles to success are more significant than physical ones.

### 3. How to build a Growth Mindset
#### Question 4
#### What are the key points mentioned by speaker to build growth mindset.

* A growth mindset begins with the
fundamental number one rule in life and
that is believe in our ability to figure things out.
* If we have a lofty goal but are unsure about how to accomplish it, we must think in a way that will help us find a solution.
* Second, we must challenge our presumptions and pessimistic beliefs.
* Thirdly, we need to build an openness to new ideas into our life curriculum.
* Finally, respect the battle since it will make us stronger. 

### 4. Mindset - A MountBlue Warrior Reference Manual
#### Question 5
#### What are your ideas to take action and build Growth Mindset?

* I will understand each concept properly.
* I will stay with a problem till I complete it. I will not quit the problem.
* I will not leave my code unfinished till I complete the following checklist:Make it work.Make it readable.Make it modular.Make it efficient.
* I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.


