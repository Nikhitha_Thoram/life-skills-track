# Focus Management

### 1. What is Deep Work
#### Question 1
### What is Deep Work?

* Deep work is the ability to focus without distraction on a cognitively demanding task.
* Deep work is like a superpower in our increasingly competitive twenty-first-century economy.
* Doing hard work with full focus for a long time is called "Deep work."

### 4. Summary of Deep Work Book
#### Question 2
### According to author how to do deep work properly, in a few points?

* schedule distractions.
* we need to set boundaries for distractions.
* We have to develop deep work ritual.
* we need to have better sleep.

#### Question 3
### How can you implement the principles in your day to day life?

* Remove distractions.
* Make deep work as a habit.
* Establish short-term and long-term goals.
* Time management.
* Surround myself with people who uplift and inspire me.

### 5. Dangers of Social Media
#### Question 4
### What are the dangers of social media, in brief?

* Reduces Face-to-Face Interaction.
* Increases Cravings for Attention.
* Distracts From Life Goals.
* Can Lead to a Higher Risk of Depression.
* Encountering Cyberbullies.
* Social Comparison Reduces Self-Esteem.
