 # Browser Rendering

 Rendering a webpage is the process of turning HTML, CSS, and JavaScript code into an interactive page that website visitors expect to see when clicking on a link.

The process can be divided into the following main stages:

* HTML Parsing
* Fetching external resources
* Parse the CSS and build the CSSOM
* Execute the JavaScript
* Merge DOM and CSSOM to construct the render tree
* Calculate layout and paint

### HTML Parsing
* Tokenization: The browser breaks down the HTML markup into tokens. Tokens represent different parts of the HTML structure, such as start tags, end tags, attribute names and values, text content, comments, etc.

* Lexing (or Lexical Analysis): The tokens generated from the tokenization stage are further analyzed to determine their type and relationship to each other. This process involves categorizing tokens and identifying their roles in the document structure.

* DOM Tree Construction: Based on the tokens and their relationships identified during the lexing stage, the browser constructs the Document Object Model (DOM) tree. This tree represents the hierarchical structure of the HTML document, with each element represented as a node in the tree.

* Parsing Scripts and Stylesheets: While parsing the HTML, the browser may encounter external resources such as JavaScript files and CSS stylesheets. If present, these resources are fetched and parsed concurrently with the HTML parsing process.

* Error Handling: Throughout the parsing process, the browser also performs error handling. It detects syntax errors, missing tags, and other issues in the HTML markup and attempts to recover from them to continue building the DOM tree.

* Incremental Parsing: In modern browsers, parsing is often performed incrementally. Instead of waiting for the entire HTML document to be downloaded before starting parsing, the browser can start processing the received data in chunks as it arrives. This allows for faster page loading and rendering, especially for large documents or slow network connections.

### Fetching external resources

* External Resource Fetching: When the browser encounters external resources like CSS or JavaScript files while parsing the HTML, it initiates requests to fetch those files from the server.

* CSS File Loading: While a CSS file is being fetched, the browser continues parsing the HTML. However, rendering is blocked until the CSS file is fully loaded and parsed, as CSS affects the layout and styling of the page.

* JavaScript File Loading: By default, JavaScript files block HTML parsing until they are loaded and parsed. However, two attributes, defer and async, can be added to script tags to modify this behavior.

  * defer: Specifies that the execution of the JavaScript file should be deferred until the HTML parsing is complete. Multiple deferred scripts are executed in the order they appear in the HTML.
  
  * async: Indicates that the JavaScript file can be executed asynchronously, allowing HTML parsing to continue while the script is being fetched. The order of execution for async scripts is not guaranteed.

  * Preloading Resources: Modern browsers may speculatively download external resources while blocked on parsing, based on a lookahead mechanism. To prioritize the loading of certain resources and indicate their importance, the link tag with rel="preload" attribute can be used.
  This tells the browser to preload the style.css file as a stylesheet, allowing it to be fetched and loaded earlier in the rendering process.

### Parse the CSS and build the CSSOM
* CSS Object Model (CSSOM):
  
  * The CSSOM is a map of all CSS selectors and their relevant properties for each selector.
  * It is structured as a tree, similar to the Document Object Model (DOM), with a root node and various relationships such as siblings, descendants, and children.
  * Like the DOM, the CSSOM is a critical component of the rendering process for web pages.
  * Together with the DOM, the CSSOM is used to build the render tree, which is essential for layout and painting the web page. 

* Parsing CSS Files:
  
  * When CSS files are loaded by the browser, they must be parsed and converted into the CSSOM.
  * The CSSOM describes all CSS selectors present on the page, their hierarchy, and their associated properties.

* Difference from DOM:
  
  * Unlike the DOM, the CSSOM cannot be built incrementally.
  * This is because CSS rules can override each other based on specificity, making it necessary to parse all CSS files and construct the complete CSSOM before rendering can occur.
  * Due to this behavior, CSS parsing and building the CSSOM block rendering, as the browser needs the complete CSSOM to determine how to position each element on the screen.

### Execute the JavaScript
* JavaScript Parsing, Compilation, and Execution:
 
   * JavaScript resources are loaded, parsed, compiled, and executed by the browser's JavaScript engine.
   * Parsing and compiling JavaScript can be resource-intensive, so optimizing this process is crucial for performance.
   * Different browsers utilize different JavaScript engines to perform these tasks.

* Load Events:
 
   * After synchronously loaded JavaScript and the DOM are fully parsed and ready, the DOMContentLoaded event is emitted.
   * Scripts that require access to the DOM should wait for the DOMContentLoaded event before executing. This ensures that the DOM is fully accessible.
   * Example:

        ````
        document.addEventListener('DOMContentLoaded', (event) => {

        // we can now safely access the DOM
        });

        ````
    * Once all resources, including async JavaScript, images, etc., have finished loading, the window.load event is fired.
    * The load event signifies that the page has fully loaded and is ready for interaction.
    * Example:
       
       ```
       window.addEventListener('load',      (event) => {
          // The page has now fully loaded
       });

       ````

* Timeline of JavaScript Execution:
  
  * JavaScript execution follows a timeline that includes loading, parsing, compilation, and execution stages.
  * Understanding and optimizing this timeline is essential for ensuring efficient web page performance.

###   Merge DOM and CSSOM to construct the render tree

* Render Tree Composition:
   
   * The render tree is a combination of the DOM (Document Object Model) and CSSOM (CSS Object Model).
   * It represents all elements that will be rendered onto the page.
   * Not all nodes in the render tree will necessarily be visually present on the page.
   * Nodes with styles like opacity: 0 or visibility: hidden may still be included in the render tree but not visibly displayed.
   * Nodes set to display: none will not be included in the render tree.
   * Tags such as head that do not contain visual information are always omitted from the render tree.

* Role of Rendering Engines:
 
  * Different browsers utilize different rendering engines to construct and manipulate the render tree.
  * Rendering engines are responsible for interpreting and executing the render tree to generate the final layout and visual representation of the webpage.
  * They handle rendering tasks such as layout calculation, painting, and compositing.
  * Examples of rendering engines include Blink (used in Google Chrome), Gecko (used in Mozilla Firefox), and WebKit (used in Safari).

###  Calculate layout and paint

* Layout Calculation: 
 
   * After the render tree is constructed, the browser needs to determine the position and size of each node in the render tree.
   * This process is known as layout calculation or reflow.
   * The rendering engine traverses the render tree, starting from the root and working down, to calculate the coordinates and dimensions of each node.
   * Layout calculation takes into account factors such as CSS box model properties (e.g., width, height, margin, padding), positioning (e.g., absolute, relative), and the flow of content within the document.

* Painting:
 
   * Once layout calculation is complete and the position and size of each node are determined, the final step is to paint the pixels onto the screen.
   * Painting involves rendering the visual representation of each node onto the display.
   * The rendering engine uses the layout information generated in the previous step to accurately paint each element at its calculated position and size.
   * accurately paint each element at its calculated position and size.
   * Painting involves operations such as filling shapes, rendering text, applying styles (colors, gradients, shadows), and handling transparency and blending.

* Completion of Rendering:

  * After painting, the browser has fully rendered the web page, and it is ready for display to the user.
  * The rendered page includes all visible elements, accurately positioned and styled according to the HTML content, CSS rules, and any dynamic changes made via JavaScript.
  * Users can now interact with the rendered page, and the browser continues to handle events, updates, and rendering as needed.


# References

 * [https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model)

 * [https://qarea.com/blog/webpage-rendering-how-it-works-tips-on-optimization](https://qarea.com/blog/webpage-rendering-how-it-works-tips-on-optimization)

 * [https://starkie.dev/blog/how-a-browser-renders-a-web-page](https://starkie.dev/blog/how-a-browser-renders-a-web-page)

